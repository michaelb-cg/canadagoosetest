﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanadaGooseTest
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities db = new NorthwindEntities("Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password = myPassword;");

            List<Customer> cs = db.Customers.ToList();

            foreach (Customer c in cs)
            {
                if(c.IsActive == false)
                {
                    DeleteLogger logger = new DeleteLogger(c);
                    logger.Log("deleted customer: " + c.Id);
                }
                else
                {
                    c.LastTouch = DateTime.Now;
                    UpdateLogger logger = new UpdateLogger();
                    logger.Log("updated customer: " + c.Id);
                }
            }
        }   
    }
}
