﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CanadaGooseTest
{
    public class DeleteLogger:UpdateLogger
    {
        private Customer _c;

        public DeleteLogger(Customer c)
        {
            NorthwindEntities db = new NorthwindEntities("Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password = myPassword;");
            List<Customer> cs = db.Customers.ToList();
            cs.Delete(c);
            _c = c;
        }

        public override void Log(string v)
        {

            StreamWriter writer = new StreamWriter(File.Open("c:\\logs\\" + DateTime.Today + "DeleteLog.log", FileMode.Append));

            writer.Write(v);

            writer.Flush();

            writer.Close();
        }
    }
}