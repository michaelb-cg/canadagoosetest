﻿using System;

namespace CanadaGooseTest
{
    public class Customer
    {
        public bool IsActive { get; internal set; }
        public int Id { get; internal set; }
        public DateTime LastTouch { get; internal set; }
    }
}