﻿using System;
using System.IO;

namespace CanadaGooseTest
{
    public class UpdateLogger
    {
        public virtual void Log(string v)
        {

            EmailAlerter alerter = new EmailAlerter("smtp://127.0.0.1", 25);

            alerter.SendEmail(v);

            StreamWriter writer = new StreamWriter(File.Open("c:\\logs\\" + DateTime.Today + ".log", FileMode.Append));

            writer.Write(v);

            writer.Flush();

            writer.Close();
        }
    }
}