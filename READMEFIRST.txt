In this zip file you can find solution for small app that will be executed on regular basis and will clean up database from all inactive users

So if user is already marked as inactive by another part of the system, 
this app will delete it, and if user still active, it will update LastTouch property to keep track when was last time customer record was checked


This code compiles but doesn't really go to database and doesn't really sends any alerts or emails


Task that we would like you to complete is to refactor code to how you believe it should be organized and written

There is no need to implement actual interaction with database or email server.

When you done with refactoring, please commit your changes locally, zip the folder and send it back to us by uploading to your favorite cloud storage (google drive, onedrive, etc.) and send us a link.

Thank you and good luck!!!

